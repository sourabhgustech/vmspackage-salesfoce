<?php
namespace VMSSalesForce\SalesForceIntegration;

/**
 * This file will have all methods related to REST APIs to Salesforce
 *
 * Most of the methods in this class are public
 *
 * 
 * @link   
 * @since  1.0
 * @author Simplify-Dev
 */

class SalesForceIntegration
{
	/**
     * Function to handle get candidates request
     *
     * @var $conditions - to search candidate info
     */
    public function getCandidates($conditions) {        
        $return = array();
        $code = 301;
        try {

        	$params = array();
        	$params['name'] = 'get_candidates';
            $conditions['lead_id'] = !empty($conditions['lead_id']) ? $conditions['lead_id'] : "";
            // we will make constant for the URL
            $url = \Config::get('constants.SalesForce.LeadURL') . '/' . $conditions['lead_id'];
            
        	$params['url'] = $url;
        	$params['params'] = '';
        	$params['headers'] = '';
        	$request = new SMRequests();
        	$response = $request->processRequest($params);

            $return = $response;
        } catch(Exception $e) {
            $return = array('status'=>'fail', 'message' => "Exception in getting lead data: ".$e->getMessage());
        }
        return $return;
    }
}