<?php
namespace VMSSalesForce\SalesForceIntegration;

/**
 * This file will have all methods related to Calling REST APIs to Salesforce
 * The methos in this file will have business logic for APIs
 * Most of the methods in this class are public
 *
 * 
 * @link   
 * @since  1.2
 * @author Simplify-Dev
 */

class SMRequests {

	// function to create API request
	public function processRequest($requestsParams)
    {	
        if($requestsParams['name'] == "get_candidates") {
            $access_token = $this->getAccessToken();
            if(!empty($access_token)) {   
	        	$requestsParams['type'] = "GET";
				$requestsParams['headers'] = [
					'Authorization: Bearer '.$access_token,
					'Accept: application/json'
				];
				
	        	return makeRequest($requestsParams);
        	} else {
        		return array('Error' => "Empty Access Token");
        	}
        }
    }

    // function get get access token
    private function getAccessToken() {
    	$requestsParams = array();    	
    	$requestsParams['url'] = env('SFDC_AUTH_URL');
		$requestsParams['type'] = "POST";
		$postfields = [
			'grant_type=password',
			'client_id=' . env('SFDC_CLIENT_ID'),
			'client_secret=' . env('SFDC_CLIENT_SECRET'),
			'username=' . env('SFDC_EMAIL'),
			'password=' . env('SFDC_PASSWORD') . env('SFDC_ACCESS_TOKEN')
		];
		$requestsParams['params'] = implode("&", $postfields);
		$requestsParams['headers'] = [
				'Content-Type: application/x-www-form-urlencoded',
				'Accept: application/json'
			];

    	$res = makeRequest($requestsParams);

        $return = '';
    	if(!empty($res)) {
    		$resArr = (array) json_decode($res);
    	
    		if(is_array($resArr) && isset($resArr['access_token'])) {
    			$return = $resArr['access_token'];
    		}
    	}
        return $return;
    }
}
